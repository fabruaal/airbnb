import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlanListarComponent } from './plan-listar/plan-listar.component';



@NgModule({
  declarations: [PlanListarComponent],
  imports: [
    CommonModule
  ]
})
export class PlanModule { }
