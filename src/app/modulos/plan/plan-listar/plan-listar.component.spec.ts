import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanListarComponent } from './plan-listar.component';

describe('PlanListarComponent', () => {
  let component: PlanListarComponent;
  let fixture: ComponentFixture<PlanListarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanListarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanListarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
