import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlojamientoListarComponent } from './alojamiento-listar/alojamiento-listar.component';



@NgModule({
  declarations: [AlojamientoListarComponent],
  imports: [
    CommonModule
  ]
})
export class AlojamientoModule { }
