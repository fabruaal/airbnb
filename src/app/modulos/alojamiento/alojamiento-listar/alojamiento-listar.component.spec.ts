import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlojamientoListarComponent } from './alojamiento-listar.component';

describe('AlojamientoListarComponent', () => {
  let component: AlojamientoListarComponent;
  let fixture: ComponentFixture<AlojamientoListarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlojamientoListarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlojamientoListarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
