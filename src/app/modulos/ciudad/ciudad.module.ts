import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CiudadListarComponent } from './ciudad-listar/ciudad-listar.component';



@NgModule({
  declarations: [CiudadListarComponent],
  imports: [
    CommonModule
  ]
})
export class CiudadModule { }
